package io.gitlab.ihsahn.netbeans.modules.toml.highlighting;

import io.gitlab.ihsahn.netbeans.modules.toml.Constants;
import java.util.Collection;
import java.util.Collections;
import org.netbeans.api.editor.mimelookup.MimeRegistration;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.spi.SchedulerTask;
import org.netbeans.modules.parsing.spi.TaskFactory;

@MimeRegistration(mimeType=Constants.MIME_TYPE,service=TaskFactory.class)
public class TomlSyntaxErrorHighlightingTaskFactory extends TaskFactory {

    @Override
    public Collection<? extends SchedulerTask> create(Snapshot snpshot) {
         return Collections.singleton (new TomlSyntaxErrorHighlightingTask());
    }

}
