package io.gitlab.ihsahn.netbeans.modules.toml.editor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import org.openide.util.Exceptions;

/**
 * @author James Reid
 */
public class AntlrTokenReader {

    private final HashMap<String, String> tokenTypes = new HashMap<>();

    public AntlrTokenReader() {
        init();
    }

    /**
     * Initializes the map
     */
    private void init() {

        tokenTypes.put("COMMENT", TokenCategories.comment.name());
        tokenTypes.put("BOOLEAN", TokenCategories.bool.name());
        tokenTypes.put("UNQUOTED_KEY", TokenCategories.keys.name());
        tokenTypes.put("OFFSET_DATE_TIME", TokenCategories.date.name());
        tokenTypes.put("LOCAL_DATE_TIME", TokenCategories.date.name());
        tokenTypes.put("LOCAL_DATE", TokenCategories.date.name());
        tokenTypes.put("LOCAL_TIME", TokenCategories.date.name());

        tokenTypes.put("'{'", TokenCategories.braces.name());
        tokenTypes.put("'}'", TokenCategories.braces.name());

        tokenTypes.put("'[['", TokenCategories.brackets.name());
        tokenTypes.put("']]'", TokenCategories.brackets.name());
        tokenTypes.put("'['", TokenCategories.brackets.name());
        tokenTypes.put("']'", TokenCategories.brackets.name());

        tokenTypes.put("FLOAT", TokenCategories.number.name());
        tokenTypes.put("INF", TokenCategories.number.name());
        tokenTypes.put("NAN", TokenCategories.number.name());
        tokenTypes.put("DEC_INT", TokenCategories.number.name());
        tokenTypes.put("HEX_INT", TokenCategories.number.name());
        tokenTypes.put("OCT_INT", TokenCategories.number.name());
        tokenTypes.put("BIN_INT", TokenCategories.number.name());

        tokenTypes.put("BASIC_STRING", TokenCategories.string.name());
        tokenTypes.put("ML_BASIC_STRING", TokenCategories.string.name());
        tokenTypes.put("LITERAL_STRING", TokenCategories.string.name());
        tokenTypes.put("ML_LITERAL_STRING", TokenCategories.string.name());
        tokenTypes.put("'='", TokenCategories.assignment.name());
        tokenTypes.put("'.'", TokenCategories.dot.name());
        tokenTypes.put("WS", TokenCategories.whitespace.name());
    }


    /**
     * Reads the token file from the ANTLR parser and generates
     * appropriate tokens.
     *
     * @return
     */
    public Map<Integer, TomlTokenId> readTokenFile() {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        InputStream inp = classLoader.getResourceAsStream("TomlGrammar.tokens");
        BufferedReader input = new BufferedReader(new InputStreamReader(inp));
        return readTokenFile(input);
    }

    /**
     * Reads in the token file.
     *
     * @param buff
     */
    private Map<Integer, TomlTokenId> readTokenFile(BufferedReader buff) {
        Map<Integer, TomlTokenId> tokenMap = new HashMap<>();
        String line;
        try {
            while ((line = buff.readLine()) != null) {
                int pos = line.lastIndexOf("=");
                String name = line.substring(0, pos);
                int tok = Integer.parseInt(line.substring(pos + 1));
                TomlTokenId id;
                String tokenCategory = tokenTypes.get(name);
                if (tokenCategory != null) {
                    //if the value exists, put it in the correct category
                    id = new TomlTokenId(name, tokenCategory, tok);
                } else {
                    //if we don't recognize the token, consider it to a separator
                    id = new TomlTokenId(name, "separator", tok);
                }
                //prevents duplicates
                tokenMap.put(tok, id);
            }
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
        return tokenMap;
    }

    enum TokenCategories {
        bool, comment, date, keys, number, braces, brackets, string, assignment, 
        dot, whitespace
    }
}
