package io.gitlab.ihsahn.netbeans.modules.toml.editor;

import java.util.Collection;
import java.util.Map;

import io.gitlab.ihsahn.netbeans.modules.toml.Constants;
import org.netbeans.spi.lexer.LanguageHierarchy;
import org.netbeans.spi.lexer.Lexer;
import org.netbeans.spi.lexer.LexerRestartInfo;

public class TomlLanguageHierarchy extends LanguageHierarchy<TomlTokenId> {

    private static Collection<TomlTokenId> tokens;
    private static Map<Integer, TomlTokenId> idToToken;

    static synchronized TomlTokenId getToken(int id) {
        if (idToToken == null) {
            init();
        }
        return idToToken.get(id);
    }

    private static void init() {
        AntlrTokenReader reader = new AntlrTokenReader();
        idToToken = reader.readTokenFile();
        tokens = idToToken.values();
    }

    @Override
    protected synchronized Collection<TomlTokenId> createTokenIds() {
        if (tokens == null) {
            init();
        }
        return tokens;
    }

    @Override
    protected Lexer<TomlTokenId> createLexer(LexerRestartInfo<TomlTokenId> info) {
        return new NetbeansTomlLexer(info);
    }

    @Override
    protected String mimeType() {
        return Constants.MIME_TYPE;
    }

}
