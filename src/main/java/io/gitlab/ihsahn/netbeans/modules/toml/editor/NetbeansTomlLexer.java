package io.gitlab.ihsahn.netbeans.modules.toml.editor;

import io.gitlab.ihsahn.netbeans.modules.toml.editor.antlr.TomlGrammarLexer;
import org.netbeans.api.lexer.Token;
import org.netbeans.spi.lexer.Lexer;
import org.netbeans.spi.lexer.LexerInput;
import org.netbeans.spi.lexer.LexerRestartInfo;
import org.netbeans.spi.lexer.TokenFactory;

public class NetbeansTomlLexer implements Lexer<TomlTokenId> {

    private final LexerInput input;
    private final TokenFactory<TomlTokenId> tokenFactory;

    TomlGrammarLexer lexer;

    public NetbeansTomlLexer(LexerRestartInfo<TomlTokenId> info) {
        this.input = info.input();
        NbLexerCharStream charStream = new NbLexerCharStream(input);
        this.tokenFactory = info.tokenFactory();
        this.lexer = new TomlGrammarLexer(charStream);
    }

    @Override
    public org.netbeans.api.lexer.Token<TomlTokenId> nextToken() {
        org.antlr.v4.runtime.Token token = lexer.nextToken();

        Token<TomlTokenId> createdToken = null;

        if (token.getType() != -1) {
            TomlTokenId tokenId = TomlLanguageHierarchy.getToken(token.getType());
            createdToken = tokenFactory.createToken(tokenId);
        } else if (input.readLength() > 0) {
            TomlTokenId tokenId = TomlLanguageHierarchy.getToken(TomlGrammarLexer.WS);
            createdToken = tokenFactory.createToken(tokenId);
        }

        return createdToken;
    }

    @Override
    public Object state() {
        return null; //no specific state
    }

    @Override
    public void release() {
        //nothing to release
    }
}
