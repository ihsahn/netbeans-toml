package io.gitlab.ihsahn.netbeans.modules.toml.editor;

import io.gitlab.ihsahn.netbeans.modules.toml.editor.antlr.TomlGrammarLexer;
import io.gitlab.ihsahn.netbeans.modules.toml.editor.antlr.TomlGrammarParser;
import java.util.LinkedList;
import java.util.List;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Task;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.SourceModificationEvent;

import javax.swing.event.ChangeListener;

public class NetbeansTomlParser extends Parser {

    private TomlParserResult lastResult;

    @Override
    public void parse(Snapshot snapshot, Task task, SourceModificationEvent sourceModificationEvent) {
        CharSequence text = snapshot.getText();

        CharStream inp = new CharSequenceCharStream(text, text.length(), snapshot.getSource().getFileObject().getNameExt());
        TomlGrammarLexer lexer = new TomlGrammarLexer(inp);
        CommonTokenStream tokenStream = new CommonTokenStream(lexer);
        TomlGrammarParser parser = new TomlGrammarParser(tokenStream);

        // remove default listeners
        parser.removeErrorListeners();
        parser.setTrace(false);
        lexer.removeErrorListeners();

        List<TomlSyntaxError> syntaxErrors = new LinkedList<>();
        TomlAntlrErrorListener errorListener = new TomlAntlrErrorListener(syntaxErrors);

        lexer.addErrorListener(errorListener);
        parser.addErrorListener(errorListener);

        TomlGrammarParser.DocumentContext document = parser.document();
        lastResult = new TomlParserResult(snapshot, document, syntaxErrors);
    }

    @Override
    public Result getResult(Task task) {
        return lastResult;
    }

    @Override
    public void addChangeListener(ChangeListener changeListener) {

    }

    @Override
    public void removeChangeListener(ChangeListener changeListener) {

    }
}
