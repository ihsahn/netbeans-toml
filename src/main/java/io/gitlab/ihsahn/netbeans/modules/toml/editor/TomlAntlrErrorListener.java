package io.gitlab.ihsahn.netbeans.modules.toml.editor;

import io.gitlab.ihsahn.netbeans.modules.toml.editor.antlr.TomlGrammarLexer;
import io.gitlab.ihsahn.netbeans.modules.toml.editor.antlr.TomlGrammarParser;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.Interval;

import java.util.List;

public class TomlAntlrErrorListener extends BaseErrorListener {

    private final List<TomlSyntaxError> errors;

    public TomlAntlrErrorListener(List<TomlSyntaxError> errors) {
        this.errors = errors;
    }

    @Override
    public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int column,
                            String errorMessage, RecognitionException re) {
        // special handling for lexer `no viable` exceptions becayse we do rewrite some of them
        if (re instanceof LexerNoViableAltException) {
            lexerNoViableAltException((TomlGrammarLexer) recognizer, errorMessage, (LexerNoViableAltException) re);
        } else if (offendingSymbol instanceof CommonToken) {
            CommonToken ct = (CommonToken) offendingSymbol;
            if ((ct.getType() == -1) && (recognizer instanceof TomlGrammarParser)) {
                errors.add(new TomlSyntaxError(errorMessage, ct.getStartIndex() - 1, ct.getStartIndex()));
            } else {
                errors.add(new TomlSyntaxError(errorMessage, ct.getStartIndex(), ct.getStartIndex() + ct.getText().length()));
            }
        }
    }

    private void lexerNoViableAltException(TomlGrammarLexer lexer, String errorMessage, LexerNoViableAltException noViableAltException) {
        CharStream inputStream = lexer.getInputStream();
        String errorDisplay = lexer.getErrorDisplay(inputStream.getText(
                new Interval(noViableAltException.getStartIndex(), inputStream.index())));
        //original message is "token recognition error at ", so let's rewrite it
        if (errorDisplay != null && errorDisplay.startsWith("\"")) {
            errors.add(new TomlSyntaxError("Unfinished double quoted string literal",
                    noViableAltException.getStartIndex(),
                    noViableAltException.getStartIndex() + errorDisplay.length()));

        } else {
            errors.add(new TomlSyntaxError(errorMessage,
                    noViableAltException.getStartIndex(),
                    noViableAltException.getStartIndex() + 1));
        }
    }
}
