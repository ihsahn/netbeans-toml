package io.gitlab.ihsahn.netbeans.modules.toml.editor;

import org.netbeans.api.lexer.TokenId;

public class TomlTokenId implements TokenId {

    private final String name;
    private final String primaryCategory;
    private final int id;

    public TomlTokenId(String name, String primaryCategory, int id) {
        this.name = name;
        this.primaryCategory = primaryCategory;
        this.id = id;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public int ordinal() {
        return id;
    }

    @Override
    public String primaryCategory() {
        return primaryCategory;
    }

}
