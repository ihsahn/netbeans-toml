package io.gitlab.ihsahn.netbeans.modules.toml;

public class Constants {

    public final static String MIME_TYPE = "application/toml";
    public final static String ICON_PATH = "io/gitlab/ihsahn/netbeans/modules/toml/toml_16x16.png";
}
