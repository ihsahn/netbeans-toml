package io.gitlab.ihsahn.netbeans.modules.toml.editor;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.misc.Interval;
import org.netbeans.spi.lexer.LexerInput;

/**
 * based on netbeans/webcommon/javascript2.lexer/src/org/netbeans/modules/javascript2/lexer/NbLexerCharStream.java
 * with fixes
 */
public class NbLexerCharStream implements CharStream {

    private static final String STREAM_NAME = "NbLexerCharStream"; //NOI18N
    private final LexerInput li;

    public NbLexerCharStream(LexerInput li) {
        this.li = li;
    }

    @Override
    public void consume() {
        read();
    }

    @Override
    public int LA(int lookahead) {
        if (lookahead == 0) {
            return 0; //the behaviour is not defined
        }

        int c = 0;
        for (int i = 0; i < lookahead; i++) {
            c = read();
        }
        li.backup(lookahead);
        return c;
    }

    @Override
    public int mark() {
        //I assume LexerInput has access to all data, so there's no need to mark anything (keep in buffer)
        //to make seek() safe
        return -1;
    }

    @Override
    public int index() {
        return li.readLengthEOF();
    }

    @Override
    public void release(int marker) {
    }

    @Override
    public void seek(int i) {
        if (i < index()) {
            //go backward
            li.backup(index() - i);
        } else {
            // go forward
            while (index() < i) {
                consume();
            }
        }
    }

    @Override
    public int size() {
        return -1; //unknown
    }

    @Override
    public String getSourceName() {
        return STREAM_NAME;
    }

    private int read() {
        int result = li.read();
        if (result == LexerInput.EOF) {
            result = CharStream.EOF;
        }

        return result;
    }

    @Override
    public String getText(Interval intrvl) {
        return li.readText(intrvl.a, intrvl.b).toString();
    }
}