package io.gitlab.ihsahn.netbeans.modules.toml.editor;

public class TomlSyntaxError {

    private final String errorMessage;
    private final int startPosition;
    private final int endPosition;

    public TomlSyntaxError(String errorMessage, int startPosition, int endPosition) {
        this.errorMessage = errorMessage;
        this.startPosition = startPosition;
        this.endPosition = endPosition;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public int getStartPosition() {
        return startPosition;
    }

    public int getEndPosition() {
        return endPosition;
    }

    @Override
    public String toString() {
        return "TomlSyntaxError{" +
                "errorMessage='" + errorMessage + '\'' +
                ", startPosition=" + startPosition +
                ", endPosition=" + endPosition +
                '}';
    }

}
