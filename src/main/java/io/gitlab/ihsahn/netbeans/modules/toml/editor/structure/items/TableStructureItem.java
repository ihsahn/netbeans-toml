package io.gitlab.ihsahn.netbeans.modules.toml.editor.structure.items;

import org.netbeans.modules.csl.api.ElementKind;
import org.openide.filesystems.FileObject;

public class TableStructureItem extends AbstractTomlStructureItem {

    public TableStructureItem(String name, Long start, FileObject fileObject, boolean isNested) {
        super(name, start, ElementKind.METHOD, fileObject, isNested);
    }
}
