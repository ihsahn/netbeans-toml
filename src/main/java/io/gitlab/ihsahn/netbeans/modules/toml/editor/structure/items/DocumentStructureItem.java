package io.gitlab.ihsahn.netbeans.modules.toml.editor.structure.items;

import org.netbeans.modules.csl.api.ElementKind;
import org.openide.filesystems.FileObject;

public class DocumentStructureItem extends AbstractTomlStructureItem {

    public DocumentStructureItem(String name, Long start, FileObject fileObject) {
        super(name, start, ElementKind.CLASS, fileObject, false);
    }
}
