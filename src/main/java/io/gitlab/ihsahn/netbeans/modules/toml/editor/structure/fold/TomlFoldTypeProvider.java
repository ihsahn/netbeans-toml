package io.gitlab.ihsahn.netbeans.modules.toml.editor.structure.fold;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import io.gitlab.ihsahn.netbeans.modules.toml.Constants;
import org.netbeans.api.editor.fold.FoldTemplate;
import org.netbeans.api.editor.fold.FoldType;
import org.netbeans.api.editor.mimelookup.MimeRegistration;
import org.netbeans.spi.editor.fold.FoldTypeProvider;
import org.openide.util.NbBundle;

@MimeRegistration(mimeType = Constants.MIME_TYPE, service = FoldTypeProvider.class)
public class TomlFoldTypeProvider implements FoldTypeProvider {

    @NbBundle.Messages("FoldType_TomlTables=Tables")
    public static final FoldType TOML_TABLE = FoldType.TAG.override(Bundle.FoldType_TomlTables(),
            new org.netbeans.api.editor.fold.FoldTemplate(1, 1, FoldTemplate.CONTENT_PLACEHOLDER)); // NOI18N

    List<FoldType> foldTypes = Collections.singletonList(TOML_TABLE);

    @Override
    public Collection getValues(Class type) {
        return foldTypes;
    }

    @Override
    public boolean inheritable() {
        return false;
    }

}
