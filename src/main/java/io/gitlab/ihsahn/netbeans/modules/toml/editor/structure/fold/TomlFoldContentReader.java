package io.gitlab.ihsahn.netbeans.modules.toml.editor.structure.fold;

import io.gitlab.ihsahn.netbeans.modules.toml.Constants;
import org.netbeans.api.editor.fold.Fold;
import org.netbeans.api.editor.fold.FoldTemplate;
import org.netbeans.api.editor.fold.FoldType;
import org.netbeans.api.editor.mimelookup.MimeRegistration;
import org.netbeans.spi.editor.fold.ContentReader;

import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

import org.netbeans.api.editor.document.LineDocument;
import org.netbeans.api.editor.document.LineDocumentUtils;

public class TomlFoldContentReader implements ContentReader {

    @Override
    public CharSequence read(Document document, Fold fold, FoldTemplate foldTemplate) throws BadLocationException {
        int lineEnd = LineDocumentUtils.getLineEnd((LineDocument) document, fold.getStartOffset());
        String text = document.getText(fold.getStartOffset(), lineEnd - fold.getStartOffset());
        int indexOf = text.indexOf(']');
        if (indexOf > -1) {
            return text.substring(0, indexOf + 1);
        }
        return null;
    }

    @MimeRegistration(mimeType = Constants.MIME_TYPE, service = ContentReader.Factory.class)
    public static class TomlFoldContentReaderFactory implements ContentReader.Factory {
        @Override
        public ContentReader createReader(FoldType ft) {
            if (ft == TomlFoldTypeProvider.TOML_TABLE) {
                return new TomlFoldContentReader();
            }
            return null;
        }
    }
}
