package io.gitlab.ihsahn.netbeans.modules.toml.highlighting;

import io.gitlab.ihsahn.netbeans.modules.toml.editor.TomlParserResult;
import io.gitlab.ihsahn.netbeans.modules.toml.editor.TomlSyntaxError;
import org.netbeans.modules.parsing.spi.ParserResultTask;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.modules.parsing.spi.SchedulerEvent;
import org.netbeans.spi.editor.hints.ErrorDescription;
import org.netbeans.spi.editor.hints.ErrorDescriptionFactory;
import org.netbeans.spi.editor.hints.HintsController;
import org.netbeans.spi.editor.hints.Severity;
import org.openide.filesystems.FileObject;

import java.util.ArrayList;
import java.util.List;

public class TomlSyntaxErrorHighlightingTask extends ParserResultTask<TomlParserResult> {

    private boolean cancelled = false;

    @Override
    public void run(TomlParserResult parserResult, SchedulerEvent se) {
        FileObject fileObject = parserResult.getSnapshot().getSource().getFileObject();
        List<ErrorDescription> errors = new ArrayList<>();
        for (TomlSyntaxError syntaxError : parserResult.getSyntaxErrors()) {
            if (cancelled) {
                return;
            }
            errors.add(ErrorDescriptionFactory.createErrorDescription(Severity.ERROR,
                    syntaxError.getErrorMessage(), fileObject, syntaxError.getStartPosition(), syntaxError.getEndPosition()));
        }

        HintsController.setErrors(fileObject, "base-toml-parser", errors);
    }

    @Override
    public int getPriority() {
        return 100;
    }

    @Override
    public Class<? extends Scheduler> getSchedulerClass() {
        return Scheduler.EDITOR_SENSITIVE_TASK_SCHEDULER;
    }

    @Override
    public void cancel() {
        cancelled = true;
    }

}
