package io.gitlab.ihsahn.netbeans.modules.toml.editor;

import io.gitlab.ihsahn.netbeans.modules.toml.editor.structure.items.AbstractTomlStructureItem;
import io.gitlab.ihsahn.netbeans.modules.toml.editor.structure.items.TableStructureItem;

import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.api.StructureItem;
import org.netbeans.modules.csl.api.StructureScanner;
import org.netbeans.modules.csl.spi.ParserResult;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class TomlStructureScanner implements StructureScanner {

    @Override
    public List<? extends StructureItem> scan(ParserResult parserResult) {
        if (parserResult == null) {
            return Collections.emptyList();
        }
        if (!(parserResult instanceof TomlParserResult)) {
            Logger.getLogger(TomlStructureScanner.class.getName()).log(Level.WARNING, "parser result of unexpected type "+parserResult.getClass().getName());
            return Collections.emptyList();

        }
        TomlParserResult tomlResult = (TomlParserResult) parserResult;
        return tomlResult.getStructure();
    }

    @Override
    public Map<String, List<OffsetRange>> folds(ParserResult parserResult) {
        if (parserResult == null) {
            return Collections.emptyMap();
        }
        if (!(parserResult instanceof TomlParserResult)) {
            Logger.getLogger(TomlStructureScanner.class.getName()).log(Level.WARNING, "parser result of unexpected type "+parserResult.getClass().getName());
            return Collections.emptyMap();

        }
        TomlParserResult tomlResult = (TomlParserResult) parserResult;
        List<AbstractTomlStructureItem> listOfRoots = tomlResult.getStructure();
        List<OffsetRange> ranges = listOfRoots.stream()
                .flatMap(AbstractTomlStructureItem::flattened)
                .filter(item -> item instanceof TableStructureItem)
                .map(item -> new OffsetRange((int) item.getPosition(), (int) item.getEndPosition()))
                .collect(Collectors.toList());
        return Collections.singletonMap("tags", ranges);
    }

    @Override
    public Configuration getConfiguration() {
        return null;
    }
}
