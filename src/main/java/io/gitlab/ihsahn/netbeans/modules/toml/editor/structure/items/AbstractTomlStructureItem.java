package io.gitlab.ihsahn.netbeans.modules.toml.editor.structure.items;

import io.gitlab.ihsahn.netbeans.modules.toml.Constants;
import org.netbeans.modules.csl.api.*;
import org.netbeans.modules.csl.spi.ParserResult;
import org.openide.filesystems.FileObject;

import javax.swing.*;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

public class AbstractTomlStructureItem implements StructureItem, ElementHandle {

    private final String name;
    private final Long start;
    private Long end;
    private final List<AbstractTomlStructureItem> children;
    private final ElementKind elementKind;
    private final FileObject fileObject;
    private final boolean requiresPrevLineAsFoldEnd;

    public AbstractTomlStructureItem(String name, Long start, ElementKind elementKind, FileObject fileObject, boolean requiresPrevLineAsFoldEnd) {
        this.name = name;
        this.start = start;
        this.elementKind = elementKind;
        this.end = start;
        this.children = new LinkedList<>();
        this.fileObject = fileObject;
        this.requiresPrevLineAsFoldEnd = requiresPrevLineAsFoldEnd;
    }

    @Override
    public FileObject getFileObject() {
        return fileObject;
    }

    @Override
    public String getMimeType() {
        return Constants.MIME_TYPE;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getIn() {
        return null;
    }

    @Override
    public String getSortText() {
        return name;
    }

    @Override
    public String getHtml(HtmlFormatter htmlFormatter) {
        return name;
    }

    @Override
    public ElementHandle getElementHandle() {
        return this;
    }

    @Override
    public ElementKind getKind() {
        return elementKind;
    }

    @Override
    public Set<Modifier> getModifiers() {
        return Collections.emptySet();
    }

    @Override
    public boolean signatureEquals(ElementHandle elementHandle) {
        if (elementHandle instanceof AbstractTomlStructureItem) {
            return name.equals(((AbstractTomlStructureItem) elementHandle).name);
        }
        return false;
    }

    @Override
    public OffsetRange getOffsetRange(ParserResult parserResult) {
        return null;
    }

    @Override
    public boolean isLeaf() {
        return getNestedItems().isEmpty();
    }

    @Override
    public List<? extends StructureItem> getNestedItems() {
        return children;
    }

    @Override
    public long getPosition() {
        return start;
    }

    @Override
    public long getEndPosition() {
        return end;
    }

    @Override
    public ImageIcon getCustomIcon() {
        return null;
    }

    public List<AbstractTomlStructureItem> getChildren() {
        return children;
    }

    public void setEnd(Long end) {
        this.end = end;
    }

    public Stream<? extends AbstractTomlStructureItem> flattened() {
        return Stream.concat(
                Stream.of(this),
                children.stream().flatMap(AbstractTomlStructureItem::flattened));
    }

    public boolean isRequiresPrevLineAsFoldEnd() {
        return requiresPrevLineAsFoldEnd;
    }

}
