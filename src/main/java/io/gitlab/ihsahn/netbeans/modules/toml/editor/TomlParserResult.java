package io.gitlab.ihsahn.netbeans.modules.toml.editor;

import io.gitlab.ihsahn.netbeans.modules.toml.editor.antlr.TomlGrammarParser;
import io.gitlab.ihsahn.netbeans.modules.toml.editor.structure.TomlStructureListener;
import io.gitlab.ihsahn.netbeans.modules.toml.editor.structure.items.AbstractTomlStructureItem;
import org.netbeans.modules.csl.api.Error;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.parsing.api.Snapshot;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

public class TomlParserResult extends ParserResult {

    private final TomlGrammarParser.DocumentContext context;
    private List<AbstractTomlStructureItem> structure;
    private Collection<TomlSyntaxError> syntaxErrors;

    protected TomlParserResult(Snapshot snapshot, TomlGrammarParser.DocumentContext context,
            Collection<TomlSyntaxError> syntaxErrors) {
        super(snapshot);
        this.context = context;
        this.syntaxErrors = syntaxErrors;
    }

    @Override
    public List<? extends Error> getDiagnostics() {
        return Collections.emptyList();
    }

    @Override
    protected void invalidate() {

    }

    synchronized List<AbstractTomlStructureItem> getStructure() {
        if (structure == null) {
            structure = scanStructure();
        }
        return structure;
    }

    private List<AbstractTomlStructureItem> scanStructure() {
        TomlStructureListener listener = new TomlStructureListener(getSnapshot().getSource());
        ParseTreeWalker.DEFAULT.walk(listener, context);
        return Collections.singletonList(listener.getRoot());
    }

    public Collection<TomlSyntaxError> getSyntaxErrors() {
        return syntaxErrors;
    }
}
