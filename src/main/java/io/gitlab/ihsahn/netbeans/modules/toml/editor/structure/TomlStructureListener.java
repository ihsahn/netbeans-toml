package io.gitlab.ihsahn.netbeans.modules.toml.editor.structure;

import io.gitlab.ihsahn.netbeans.modules.toml.editor.antlr.TomlGrammarBaseListener;
import io.gitlab.ihsahn.netbeans.modules.toml.editor.antlr.TomlGrammarParser;
import io.gitlab.ihsahn.netbeans.modules.toml.editor.structure.items.DocumentStructureItem;
import io.gitlab.ihsahn.netbeans.modules.toml.editor.structure.items.TableStructureItem;
import org.netbeans.modules.parsing.api.Source;
import org.openide.filesystems.FileObject;

import java.util.ArrayDeque;
import java.util.Deque;
import javax.swing.text.Document;
import org.netbeans.api.editor.document.LineDocument;
import org.netbeans.api.editor.document.LineDocumentUtils;

public class TomlStructureListener extends TomlGrammarBaseListener {

    private DocumentStructureItem root;
    private final FileObject file;
    private final Deque<TableStructureItem> nestedStructureItems = new ArrayDeque<>();
    // we need this so we can get end position & find line starts etc
    private final Document document;

    public TomlStructureListener(Source source) {
        this.file = source.getFileObject();
        this.document = source.getDocument(false);
    }

    public DocumentStructureItem getRoot() {
        return root;
    }

    @Override
    public void enterDocument(TomlGrammarParser.DocumentContext ctx) {
        root = new DocumentStructureItem(file.getName(), (long) ctx.start.getStartIndex(), file);
        super.enterDocument(ctx);
    }

    @Override
    public void exitDocument(TomlGrammarParser.DocumentContext ctx) {
        long documentEndPosition = document.getLength();
        //if there are any open elements we need to close them
        //let's set their end to document's end
        while (!nestedStructureItems.isEmpty()) {
            TableStructureItem item = nestedStructureItems.pollLast();
            item.setEnd(documentEndPosition);
        }
        root.setEnd(documentEndPosition);
        super.exitDocument(ctx);
    }

    @Override
    public void enterTable(TomlGrammarParser.TableContext ctx) {
        if (!nestedStructureItems.isEmpty()) {
            //we just started new table, but there are some other(s) open
            //traverse up the deque and close all that are not parent of current one
            while (!nestedStructureItems.isEmpty() && 
                    (!ctx.getText().startsWith(extractTextForMatching(nestedStructureItems.peekLast().getName())))) {
                TableStructureItem item = nestedStructureItems.pollLast();
                long previousLineEnd;
                if (item.isRequiresPrevLineAsFoldEnd()) {
                    previousLineEnd = LineDocumentUtils.getLineStart((LineDocument) document, ctx.start.getStartIndex() - 1);
                } else {
                    previousLineEnd = (long) (ctx.start.getStartIndex() - 1);
                }
                item.setEnd(previousLineEnd);
            }
        }

        //note: nested tables will require "previous line" as their fold end (in case of consecutive table exists)
        TableStructureItem item = new TableStructureItem(ctx.getText(), (long) ctx.start.getStartIndex(), file,
                !nestedStructureItems.isEmpty());
        if (!nestedStructureItems.isEmpty()) {
            nestedStructureItems.peekLast().getChildren().add(item);
        } else {
            root.getChildren().add(item);
        }
        nestedStructureItems.add(item);
        super.enterTable(ctx);
    }

    private String extractTextForMatching(String name) {
        return name.substring(0, name.length() - 1);
    }
}
