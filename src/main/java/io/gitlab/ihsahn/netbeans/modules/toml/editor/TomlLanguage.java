package io.gitlab.ihsahn.netbeans.modules.toml.editor;

import io.gitlab.ihsahn.netbeans.modules.toml.Constants;
import org.netbeans.api.lexer.Language;
import org.netbeans.modules.csl.api.StructureScanner;
import org.netbeans.modules.csl.spi.DefaultLanguageConfig;
import org.netbeans.modules.csl.spi.LanguageRegistration;
import org.netbeans.modules.parsing.spi.Parser;

@LanguageRegistration(mimeType = Constants.MIME_TYPE)
public class TomlLanguage extends DefaultLanguageConfig {

    public static final TomlLanguageHierarchy LANGUAGE_HIERARCHY = new TomlLanguageHierarchy();

    @Override
    public Language<TomlTokenId> getLexerLanguage() {
        return LANGUAGE_HIERARCHY.language();
    }

    @Override
    public String getPreferredExtension() {
        return "toml";
    }

    @Override
    public String getDisplayName() {
        return "Toml Language";
    }

    @Override
    public StructureScanner getStructureScanner() {
        return new TomlStructureScanner();
    }

    @Override
    public boolean hasStructureScanner() {
        return true;
    }

    @Override
    public Parser getParser() {
        return new NetbeansTomlParser();
    }

}
