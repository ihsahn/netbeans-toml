@TemplateRegistration(folder = "Other", content = "TomlFileTemplate.toml",
        displayName = "Toml File", requireProject = false)
package io.gitlab.ihsahn.netbeans.modules.toml;

import org.netbeans.api.templates.TemplateRegistration;
