#WIP NetBeans TOML lang plugin


[NetBeans](https://netbeans.org/) plugin for [Tom's Obvious, Minimal Language](https://github.com/toml-lang/toml). 
Compatible with NetBeans 11.2+.

Features:
- [X] file type recognition (by extension)
- [X] configurable syntax coloring
- [X] `new file` wizard template
- [X] breadcrums in editor
- [X] tables folding
- [X] syntax errors highlight in editor

more to come...

Plugin will get published once it reaches MVP feature set.

Development walkthrough covered by a blog (in Polish language) [series](https://ihsahn.gitlab.io/tags/netbeans-toml-plugin/)
